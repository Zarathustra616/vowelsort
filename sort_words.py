from vowe_count import vowel_count


def sort_words(words, number):
    word_list = []

    for i in range(number):
        word_list.append((vowel_count(words[i]), words[i]))

    word_list.sort()

    for i in range(len(word_list)):
        print(word_list[i][1], end=" ")
