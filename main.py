from sort_words import sort_words

if __name__ == "__main__":
    sentence = ["Now", "each", "day", "brings", "a", "storm"]
    n = len(sentence)

    sort_words(sentence, n)
