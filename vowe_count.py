def vowel_count(letters):
    count = 0
    vowel = set("aeiouyAEIOUY")

    for letter in letters:
        if letter in vowel:
            count = count + 1
    return count
